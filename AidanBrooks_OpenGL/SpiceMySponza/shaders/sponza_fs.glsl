#version 330

/*=---Uniforms & Structs---=*/ 
struct Material
{
	vec3 ambient_colour;
	vec3 diffuse_colour;
	bool has_diffuse_texture;
	sampler2D diffuse_texture;
	vec3 specular_colour;
	bool has_specular_texture;
	sampler2D specular_texture;
	float shininess;
};
uniform Material mat;
struct Light {
	vec3 intensity;
	vec3 position;
	float range;
};
uniform Light light[22];
uniform int numLights;
uniform vec3 Ambient_Light;
/*=---IN---=*/
in vec3 varying_position;
in vec3 varying_normal;
in vec2 varying_texcoord;
/*=---OUT---=*/
out vec4 fragment_colour;

void main(void)
{
	//Setup Sums with Default Values

	vec3 Light_Total;
	vec3 P = varying_position;
	vec3 N = normalize(varying_normal);
	vec3 Total_lights = vec3(0);
	
	for(int l = 0; l < numLights; l++)
	{
		vec3 L = normalize(light[l].position - P);
		vec3 R = reflect(-L,N);
		vec3 H = normalize(-P);
		float reflection = pow(max(dot(H,R),0.0),mat.shininess);
		
		vec3 Distance = light[l].position - P;
		float D = length(Distance);
		
		float attenuation = smoothstep(light[l].range,light[l].range / 2, D);

		float diffuse_intensity = max(0.0, dot(L, N));

		vec3 m_diffusetexture;
		vec3 m_spectexture;

		if(mat.has_diffuse_texture == true)
		{
		m_diffusetexture = mat.diffuse_colour * texture(mat.diffuse_texture,varying_texcoord).rgb;	
				
		}
		else
		{
			m_diffusetexture = mat.diffuse_colour;
		}
		if(mat.has_specular_texture == true)
		{
			m_spectexture = mat.specular_colour * texture(mat.specular_texture,varying_texcoord).rgb;
		}
		else
		{
			m_spectexture = mat.specular_colour;
		}
		if(mat.shininess == 0)
		{
		 Total_lights += attenuation * vec3(light[l].intensity * (diffuse_intensity * m_diffusetexture));
		}
		else
		{
		Total_lights += attenuation * vec3(light[l].intensity * (diffuse_intensity * m_diffusetexture) +(m_spectexture * reflection));
		}
	}
Total_lights+= Ambient_Light * mat.ambient_colour;
 fragment_colour = vec4(Total_lights,1.0);

}

	


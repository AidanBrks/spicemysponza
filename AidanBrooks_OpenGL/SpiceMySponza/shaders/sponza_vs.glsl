#version 330

// TODO: add uniforms to take in the matrix

// TODO: add in variables for each of the streamed attributes

// TODO: specify out variables to be varied to the FS

/*=---Uniforms---=*/
uniform mat4 projection_view_model_xform;
uniform mat4 model_xform;

/*=---IN---=*/
in vec3 vertex_position;
in vec3 vertex_normal;
in vec2 vertex_texturecoords;

/*=---OUT---=*/
out vec3 varying_position;
out vec3 varying_normal;
out vec2 varying_texcoord;

void main(void)
{
	varying_texcoord = vertex_texturecoords;

	varying_position = (model_xform * vec4(vertex_position,1.0)).xyz;
	varying_normal = (model_xform * vec4(vertex_normal,0.0)).xyz;

	 gl_Position = projection_view_model_xform * model_xform * vec4(vertex_position, 1.0);



}

#include "MyView.hpp"
#include <sponza/sponza.hpp>
#include <tygra/FileHelper.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
//#include <cassert>

MyView::MyView()
{
}

MyView::~MyView() {
}

void MyView::setScene(const sponza::Context * scene)
{
	scene_ = scene;
}

void MyView::windowViewWillStart(tygra::Window * window)
{
	assert(scene_ != nullptr);

	GLint compile_status = GL_FALSE;

	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	std::string vertex_shader_string
		= tygra::createStringFromFile("resource:///sponza_vs.glsl");
	const char * vertex_shader_code = vertex_shader_string.c_str();
	glShaderSource(vertex_shader, 1,
		(const GLchar **)&vertex_shader_code, NULL);
	glCompileShader(vertex_shader);
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &compile_status);
	if (compile_status != GL_TRUE)
	{
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetShaderInfoLog(vertex_shader, string_length, NULL, log);
		std::cerr << log << std::endl;
	}

	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	std::string fragment_shader_string
		= tygra::createStringFromFile("resource:///sponza_fs.glsl");
	const char * fragment_shader_code = fragment_shader_string.c_str();
	glShaderSource(fragment_shader, 1,
		(const GLchar **)&fragment_shader_code, NULL);
	glCompileShader(fragment_shader);
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compile_status);
	if (compile_status != GL_TRUE)
	{
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetShaderInfoLog(fragment_shader, string_length, NULL, log);
		std::cerr << log << std::endl;
	}

	// Create shader program & shader in variables
	shader_program_ = glCreateProgram();
	glAttachShader(shader_program_, vertex_shader);
	// TODO: glBindAttribLocation for all shader streamed IN variables e.g.
	//glBindAttribLocation(shader_program_, kVertexPosition, "vertex_position");
	glBindAttribLocation(shader_program_, kVertexPosition, "vertex_position");
	glBindAttribLocation(shader_program_, kVertexTextureCoords, "vertex_texturecoords");
	glBindAttribLocation(shader_program_, kVertexNormal, "vertex_normal");

	glDeleteShader(vertex_shader);
	glAttachShader(shader_program_, fragment_shader);
	glDeleteShader(fragment_shader);
	glLinkProgram(shader_program_);

	GLint link_status = GL_FALSE;
	glGetProgramiv(shader_program_, GL_LINK_STATUS, &link_status);
	if (link_status != GL_TRUE)
	{
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetProgramInfoLog(shader_program_, string_length, NULL, log);
		std::cerr << log << std::endl;
	}

	/*
		The framework provides a builder class that allows access to all the mesh data
	*/

	sponza::GeometryBuilder builder;
	const auto& source_meshes = builder.getAllMeshes();

	// We can loop through each mesh in the scene
	for each (const sponza::Mesh& source in source_meshes)
	{
		
		newMesh.meshId = source.getId();

		// Each mesh has an id that you will need to remember for later use
		// obained by calling source.getId()
		// To access the actual mesh raw data we can get the array e.g.
		// TODO: you also need to get the normals, elements and texture coordinates in a similar way

		const auto& positions = source.getPositionArray();
		const auto& texturecoords = source.getTextureCoordinateArray();
		const auto& elements = source.getElementArray();
		const auto& normals = source.getNormalArray();
	

		// TODO:
		// Create VBOs for position, normals, elements and texture coordinates
		// TODO
		// Create a VAO to wrap all the VBOs


		/*=---VBO---=*/

		//Position Vertex Buffer Object
		glGenBuffers(1, &newMesh.position_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, newMesh.position_vbo);
		glBufferData(GL_ARRAY_BUFFER,
			positions.size() * sizeof(glm::vec3),
			positions.data(),
			GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, kNullId);

		//Texture Coords Vertex Buffer Object
		glGenBuffers(1, &newMesh.texturecoords_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, newMesh.texturecoords_vbo);
		glBufferData(GL_ARRAY_BUFFER,
			texturecoords.size() * sizeof(glm::vec2),
			texturecoords.data(),
			GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, kNullId);

		//Element Vertex Buffer Object
		glGenBuffers(1, &newMesh.element_vbo);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, newMesh.element_vbo);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER,
			elements.size() * sizeof(unsigned int),
			elements.data(),
			GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, kNullId);
		newMesh.numElements = (int)elements.size();

		//Normal Vertex Buffer Object
		glGenBuffers(1, &newMesh.normal_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, newMesh.normal_vbo);
		glBufferData(GL_ARRAY_BUFFER,
			normals.size() * sizeof(glm::vec3),
			normals.data(),
			GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, kNullId);

		//Vertex Array Object
		glGenVertexArrays(1, &newMesh.vao);
		glBindVertexArray(newMesh.vao);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, newMesh.element_vbo);
		glBindBuffer(GL_ARRAY_BUFFER, newMesh.position_vbo);
		glEnableVertexAttribArray(kVertexPosition);
		glVertexAttribPointer(kVertexPosition, 3, GL_FLOAT, GL_FALSE,
			sizeof(glm::vec3), TGL_BUFFER_OFFSET(0));
		

		glBindBuffer(GL_ARRAY_BUFFER, newMesh.texturecoords_vbo);
		glEnableVertexAttribArray(kVertexTextureCoords);
		glVertexAttribPointer(kVertexTextureCoords, 2, GL_FLOAT, GL_FALSE,
			sizeof(glm::vec2), TGL_BUFFER_OFFSET(0));

		glBindBuffer(GL_ARRAY_BUFFER, newMesh.normal_vbo);
		glEnableVertexAttribArray(kVertexNormal);
		glVertexAttribPointer(kVertexNormal, 3, GL_FLOAT, GL_FALSE,
			sizeof(glm::vec3), TGL_BUFFER_OFFSET(0));
		
		glBindBuffer(GL_ARRAY_BUFFER, kNullId);
		glBindVertexArray(kNullId);
		
		/*=---GEN TEXTURES----=*/
		tygra::Image diff0_image
			= tygra::createImageFromPngFile("resource:///diff0.png");
		if (diff0_image.doesContainData()) {
			glGenTextures(1, &diff0_texture);
			glBindTexture(GL_TEXTURE_2D, diff0_texture);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
				GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			GLenum pixel_formats[] = { 0, GL_RED, GL_RG, GL_RGB, GL_RGBA };
			glTexImage2D(GL_TEXTURE_2D,
				0,
				GL_RGBA,
				diff0_image.width(),
				diff0_image.height(),
				0,
				pixel_formats[diff0_image.componentsPerPixel()],
				diff0_image.bytesPerComponent() == 1
				? GL_UNSIGNED_BYTE : GL_UNSIGNED_SHORT,
				diff0_image.pixelData());
			glGenerateMipmap(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, kNullId);
		}
		tygra::Image diff1_image
			= tygra::createImageFromPngFile("resource:///diff1.png");
		if (diff1_image.doesContainData()) {
			glGenTextures(1, &diff1_texture);
			glBindTexture(GL_TEXTURE_2D, diff1_texture);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
				GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			GLenum pixel_formats[] = { 0, GL_RED, GL_RG, GL_RGB, GL_RGBA };
			glTexImage2D(GL_TEXTURE_2D,
				0,
				GL_RGBA,
				diff1_image.width(),
				diff1_image.height(),
				0,
				pixel_formats[diff1_image.componentsPerPixel()],
				diff1_image.bytesPerComponent() == 1
				? GL_UNSIGNED_BYTE : GL_UNSIGNED_SHORT,
				diff1_image.pixelData());
			glGenerateMipmap(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, kNullId);
		}
		tygra::Image spec0_image
			= tygra::createImageFromPngFile("resource:///spec1.png");
		if (spec0_image.doesContainData()) {
			glGenTextures(1, &spec0_texture);
			glBindTexture(GL_TEXTURE_2D, spec0_texture);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
				GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			GLenum pixel_formats[] = { 0, GL_RED, GL_RG, GL_RGB, GL_RGBA };
			glTexImage2D(GL_TEXTURE_2D,
				0,
				GL_RGBA,
				spec0_image.width(),
				spec0_image.height(),
				0,
				pixel_formats[spec0_image.componentsPerPixel()],
				spec0_image.bytesPerComponent() == 1
				? GL_UNSIGNED_BYTE : GL_UNSIGNED_SHORT,
				spec0_image.pixelData());
			glGenerateMipmap(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, kNullId);
		}
		tygra::Image spec1_image
			= tygra::createImageFromPngFile("resource:///spec2.png");
		if (spec1_image.doesContainData()) {
			glGenTextures(1, &spec1_texture);
			glBindTexture(GL_TEXTURE_2D, spec1_texture);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
				GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			GLenum pixel_formats[] = { 0, GL_RED, GL_RG, GL_RGB, GL_RGBA };
			glTexImage2D(GL_TEXTURE_2D,
				0,
				GL_RGBA,
				spec1_image.width(),
				spec1_image.height(),
				0,
				pixel_formats[spec1_image.componentsPerPixel()],
				spec1_image.bytesPerComponent() == 1
				? GL_UNSIGNED_BYTE : GL_UNSIGNED_SHORT,
				spec0_image.pixelData());
			glGenerateMipmap(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D, kNullId);
		}

		// TODO: store in a mesh structure and add to a container for later use
		m_meshVector.push_back(newMesh);
	}

	

}

void MyView::windowViewDidReset(tygra::Window * window,
	int width,
	int height)
{
	glViewport(0, 0, width, height);
}

void MyView::windowViewDidStop(tygra::Window * window)
{
	/*=---DELETE BUFFERS---=*/
	//glDeleteProgram(shader_program_);
	//glDeleteBuffers(1, &newMesh.element_vbo);
	//glDeleteBuffers(1, &newMesh.position_vbo);
	//glDeleteBuffers(1, &newMesh.texturecoords_vbo);
	//glDeleteBuffers(1, &newMesh.normal_vbo);
	//glDeleteBuffers(1, &newMesh.vao);
}

void MyView::windowViewRender(tygra::Window * window)
{
	assert(scene_ != nullptr);

	// Configure pipeline settings
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);

	// Clear buffers from previous frame
	glClearColor(0.f, 0.f, 0.25f, 0.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glUseProgram(shader_program_);

	// Compute viewport
	GLint viewport_size[4];
	glGetIntegerv(GL_VIEWPORT, viewport_size);
	const float aspect_ratio = viewport_size[2] / (float)viewport_size[3];

	// Note: the code above is supplied for you and already works

	// TODO: Compute projection matrix
	// You can get the far plane distance, the near plane distance and the fov from
	// scene_->GetCamera().
	//glm::mat4 projection_xform = glm::perspective(glm::radians(fov), aspect_ratio, near_plane_dist, far_plane_dist);
	
	/*=---PROJECTION_XFORM---=*/
	const auto& far_plane_dist = (const float)scene_->getCamera().getFarPlaneDistance();
	const auto& near_plane_dist = (const float)scene_->getCamera().getNearPlaneDistance();
	const auto& fov = (const float)scene_->getCamera().getVerticalFieldOfViewInDegrees();
	glm::mat4 projection_xform = glm::perspective(glm::radians(fov), aspect_ratio, near_plane_dist, far_plane_dist);

	// TODO: Compute view matrix
	// You can get the camera position, look at and world up from the scene e.g.
	//const auto& camera_pos = (const glm::vec3&)scene_->getCamera().getPosition();
		// Compute camera view matrix and combine with projection matrix
	//glm::mat4 view_xform = glm::lookAt(camera_pos, camera_at_pos, world_up);


	/*=---VIEW_XFORM---=*/
	const auto& camera_pos = (const glm::vec3&)scene_->getCamera().getPosition();
	const auto& camera_at_pos = (const glm::vec3&)scene_->getCamera().getPosition() + (const glm::vec3&) scene_->getCamera().getDirection();
	const auto& world_up = (const glm::vec3&)scene_->getUpDirection();
	glm::mat4 view_xform = glm::lookAt(camera_pos, camera_at_pos, world_up);

	// TODO: create combined view * projection matrix and pass to shader as a uniform

	/*=---PROJECTION_VIEW_MODEL_XFORM---=*/
	glm::mat4 projection_view_model_xform = projection_xform * view_xform;
	GLuint projection_view_model_xform_id = glGetUniformLocation(shader_program_,
		"projection_view_model_xform");
	glUniformMatrix4fv(projection_view_model_xform_id,
		1, GL_FALSE, glm::value_ptr(projection_view_model_xform));

	// TODO: Get light data from scene via scene_->getAllLights()
	// then plug the values into the shader - you may want to leave this until you have a basic scene showing

/*=---LIGHTS---=*/
const auto& Lights = scene_->getAllLights();
const int& numLights = (const int&)Lights.size();
// Add NumLights to FS
GLuint numlights_id = glGetUniformLocation(shader_program_, "numLights");
glUniform1i(numlights_id, numLights);

for (int l = 0; l < numLights; l++)
{
	//Light Intensity
	GLuint LightIntensity_id = glGetUniformLocation(shader_program_, ("light["+ std::to_string(l) +"].intensity").c_str());	
	glUniform3fv(LightIntensity_id, 1, glm::value_ptr((const glm::vec3&)Lights[l].getIntensity()));
	//Light Position	
	GLuint LightPosition_id = glGetUniformLocation(shader_program_, ("light["+ std::to_string(l) +"].position").c_str());
		glUniform3fv(LightPosition_id, 1, glm::value_ptr((const glm::vec3&)Lights[l].getPosition()));
	//Light Range
	GLuint LightRange_id = glGetUniformLocation(shader_program_, ("light["+ std::to_string(l) +"].range").c_str());
		glUniform1f(LightRange_id, (const float&)Lights[l].getRange());
}
//Ambient Intensity
const glm::vec3 ambientintenstity = (const glm::vec3&)scene_->getAmbientLightIntensity();
GLuint AmbientLight_id = glGetUniformLocation(shader_program_, "Ambient_Light");
glUniform3fv(AmbientLight_id, 1, glm::value_ptr((const glm::vec3&)ambientintenstity));

	// TODO: Render each mesh
	// Loop through your mesh container e.g.
	//for (const auto& mesh : m_meshVector)

	for (const auto& newMesh : m_meshVector)
	{
		// Each mesh can be repeated in the scene so we need to ask the scene for all instances of the mesh
		// and render each instance with its own model matrix
		// To get the instances we need to use the meshId we stored earlier e.g.
		//const auto& instances = scene_->getInstancesByMeshId(mesh.meshId);
		// then loop through all instances
			// for each instance you can call getTransformationMatrix 
			// this then needs passing to the shader as a uniform
		const auto& instances = scene_->getInstancesByMeshId(newMesh.meshId);

		for (size_t i = 0; i < instances.size(); i++)
		{
			/*=---MODEL_XFORM---=*/
			glm::mat4 model_xform = (const glm::mat4x3&)scene_->getInstanceById(instances.at(i)).getTransformationMatrix();
			GLuint model_xform_id = glGetUniformLocation(shader_program_,
				"model_xform");
			glUniformMatrix4fv(model_xform_id,
				1, GL_FALSE, glm::value_ptr(model_xform));
			
			/*=---Material---=*/
			const auto& material_id = scene_->getInstanceById(instances.at(i)).getMaterialId();
			const auto& material = scene_->getMaterialById(material_id);

			//Ambient Colour
			const glm::vec3 ambient_colour = (const glm::vec3&)material.getAmbientColour();
			GLuint ambient_colour_id = glGetUniformLocation(shader_program_, "mat.ambient_colour");
			glUniform3fv(ambient_colour_id, 1, glm::value_ptr((const glm::vec3&)ambient_colour));
			//Diffuse Colour
			const glm::vec3 diffuse_colour = (const glm::vec3&)material.getDiffuseColour();
			GLuint diffuse_colour_id = glGetUniformLocation(shader_program_, "mat.diffuse_colour");
			glUniform3fv(diffuse_colour_id, 1, glm::value_ptr((const glm::vec3&)diffuse_colour));
			//Specular Colour
			const glm::vec3 specular_colour = (const glm::vec3&)material.getSpecularColour();
			GLuint specular_colour_id = glGetUniformLocation(shader_program_, "mat.specular_colour");
			glUniform3fv(specular_colour_id, 1, glm::value_ptr((const glm::vec3&)specular_colour));
			//Shininess
			const float shininess = (const float&)material.getShininess();
			GLuint shininess_id = glGetUniformLocation(shader_program_, "mat.shininess");
			glUniform1f(shininess_id, (const float&)shininess);

			if(material.getDiffuseTexture() == "diff0.png")
			{ 
				const bool& has_diffuse_texture = true;
				glUniform1i(glGetUniformLocation(shader_program_, "mat.has_diffuse_texture"), has_diffuse_texture);

				glActiveTexture(GL_TEXTURE0 + kTexture01);
				glBindTexture(GL_TEXTURE_2D, diff0_texture);
				glUniform1i(glGetUniformLocation(shader_program_, "mat.diffuse_texture"), kTexture01);


			}
			else if(material.getDiffuseTexture() == "diff1.png")
			{
				const bool& has_diffuse_texture = true;
				glUniform1i(glGetUniformLocation(shader_program_, "mat.has_diffuse_texture"), has_diffuse_texture);

				glActiveTexture(GL_TEXTURE0 + kTexture02);
				glBindTexture(GL_TEXTURE_2D, diff1_texture);
				glUniform1i(glGetUniformLocation(shader_program_, "mat.diffuse_texture"), kTexture02);
				
				
			}
			else
			{
				const bool& has_diffuse_texture = false;
				glUniform1i(glGetUniformLocation(shader_program_, "mat.has_diffuse_texture"), has_diffuse_texture);
				//Don't Bind Texture
			}
			if (material.getSpecularTexture() == "spec1.png")
			{
				const bool& has_specular_texture = true;
				glUniform1i(glGetUniformLocation(shader_program_, "mat.has_specular_texture"), has_specular_texture);

				glActiveTexture(GL_TEXTURE1 + kTexture03);
				glBindTexture(GL_TEXTURE_2D, spec0_texture);
				glUniform1i(glGetUniformLocation(shader_program_, "mat.specular_texture"), kTexture03);



			}
			else if(material.getSpecularTexture() == "spec2.png") 
			{
				const bool& has_specular_texture = true;
				glUniform1i(glGetUniformLocation(shader_program_, "mat.has_specular_texture"), has_specular_texture);
				glActiveTexture(GL_TEXTURE1 + KTexture04);
				glBindTexture(GL_TEXTURE_2D, spec1_texture);
				glUniform1i(glGetUniformLocation(shader_program_, "mat.specular_texture"), KTexture04);

				

			}
			else
			{
				const bool& has_specular_texture = false;
				glUniform1i(glGetUniformLocation(shader_program_, "mat.has_specular_texture"), has_specular_texture);
				//Don't Bind Texture
			}
			/*=---Bind and Render---=*/
			glBindVertexArray(newMesh.vao);
			glDrawElements(GL_TRIANGLES, newMesh.numElements, GL_UNSIGNED_INT, 0);
		}
		// Materials - leave this until you get the main scene showing
		// Each instance of the mesh has its own material accessed like so:
		// Get material for this instance
		//const auto& material_id = scene_->getInstanceById(instances.at(i)).getMaterialId();
		//const auto& material = scene_->getMaterialById(material_id);
		// You can then get the material colours from material.XX - you need to pass these to the shader

		// Finally you render the mesh e.g.
		//glDrawElements(GL_TRIANGLES, mesh.numElements, GL_UNSIGNED_INT, 0);				
	}
}

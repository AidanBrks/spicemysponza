#pragma once

#include <sponza/sponza_fwd.hpp>
#include <tygra/WindowViewDelegate.hpp>
#include <tgl/tgl.h>
#include <glm/glm.hpp>

#include <vector>
#include <memory>


class MyView : public tygra::WindowViewDelegate
{
public:

	MyView();

	~MyView();

	void setScene(const sponza::Context * scene);

private:

	void windowViewWillStart(tygra::Window * window) override;

	void windowViewDidReset(tygra::Window * window,
		int width,
		int height) override;

	void windowViewDidStop(tygra::Window * window) override;

	void windowViewRender(tygra::Window * window) override;

private:

	const sponza::Context * scene_;

	// Me from here down
	GLuint shader_program_{ 0 };
	const static GLuint kNullId = 0;
	// TODO: define values for your Vertex attributes
	enum VertexAttribIndexes {
		kVertexPosition = 0,
		kVertexTextureCoords = 1,
		kVertexNormal = 2
		
	};
	GLuint diff0_texture{ 0 };
	GLuint diff1_texture{ 0 };
	GLuint spec0_texture{ 0 };
	GLuint spec1_texture{ 0 };

	enum TextureDataIndexes {
		kTexture01 = 0,
		kTexture02 = 1,
		kTexture03 = 2,
		KTexture04 = 3
	};

	// TODO: create a mesh structure to hold VBO ids etc.
	struct Mesh
	{
		GLuint position_vbo{ 0 };
		GLuint texturecoords_vbo{ 0 };
		GLuint element_vbo{ 0 };
		GLuint normal_vbo{ 0 };
		GLuint vao{ 0 };
		int numElements{ 0 };
		int meshId{ 0 };
	};
	Mesh newMesh;
	// TODO: create a container of these mesh e.g.
	std::vector<Mesh> m_meshVector;
};
